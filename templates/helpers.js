//#DEV

var hbHelperCheckString = function(string) {
	if (string.slice(0, 2) === '%%') return true
		else return false
}

var hbHelperAdvancedString = function(string) {
	if (string.slice(0, 3) === '%%%') {
		return string.slice(3, string.length);
	} else {
		return hbHelperRandom(string);
	}
}

var hbHelperRandom = function(string) {
	string = string.slice(2, string.length);
	switch (string) {
		case 'fullname': return rFullName(string);
	}
}

//#END DEV

Handlebars.registerHelper('$make', function(partial, data, title, value, ghost, input_style, style) {
	var resources = top.$.Z.Resources.Raw();

	var notFound = function(resourceName) {
		console.warn("Resource " + resourceName + " not found");
		console.log(top.$.Z.Resources.Raw());
	}

	if (data == undefined) {
		data = {};
	}
	if (!isNaN(ghost)) {
		data.ghost = ghost;
	}

	if (title && (typeof title == 'string' || title instanceof String)) {
		if (resources[title]) data.title = resources[title];
		else if (hbHelperCheckString(title) != true) notFound(title);
	}

	if (value && (typeof value == 'string' || value instanceof String)) {
		if (resources[value]) data.value = resources[value];
		else if (hbHelperCheckString(value) != true) notFound(value);
	}

	//#DEV

	if (title && (typeof title == 'string' || title instanceof String)) {
		if (hbHelperCheckString(title) == true) data.title = hbHelperAdvancedString(title);
	}

	if (value && (typeof value == 'string' || value instanceof String)) {
		if (hbHelperCheckString(value) == true) data.value = hbHelperAdvancedString(value);
	}

	//#END DEV

	if (data.z_resource_value) {
		if (resources[data.z_resource_value]) data.value = resources[data.z_resource_value];
		else notFound(data.z_resource_value);
	}
	if (data.z_resource_title) {
		if (resources[data.z_resource_title]) data.title = resources[data.z_resource_title];
		else notFound(data.z_resource_title);
	}
	if (data.z_resource_prefix) {
		if (resources[data.z_resource_prefix]) data.prefix = resources[data.z_resource_prefix];
		else notFound(data.z_resource_prefix);
	}
	if (data.z_resource_suffix) {
		if (resources[data.z_resource_suffix]) data.suffix = resources[data.z_resource_suffix];
		else notFound(data.z_resource_suffix);
	}
	if (data.z_resource_comment) {
		if (resources[data.z_resource_comment]) data.comment = resources[data.z_resource_comment];
		else notFound(data.z_resource_comment);
	}

	// deprecated, используется только в превьюшках
	if (data.z_resource_unit) {
		if (resources[data.z_resource_unit]) data.unit = resources[data.z_resource_unit];
		else notFound(data.z_resource_unit);
	}

	if (style && (typeof style == 'string' || style instanceof String)) {
		data.style = style;
	} else {
		data.style = 'day'
	}

	if (input_style && (typeof input_style == 'string' || input_style instanceof String)) {
		data.input_style = input_style;
		if (input_style == 'number' || input_style == 'time' || input_style == 'date') {
			data.input_type = 'text';
			data.input_pattern = '[0-9]*';
		} else if (input_style == 'ip') {
			data.input_type = 'text';
		} else {
			data.input_type = input_style;
		}
	}

	console.warn('$making ' + partial + ' — $make is deprecated, use $mk instead: http://alextewpin.bitbucket.org/pages/hb-dev-mk.html')

	partial = Handlebars.partials[partial];
	compile = Handlebars.compile(partial)(data);
	output = new Handlebars.SafeString(compile);
	output.string = output.string.trim();

	return output;
});

Handlebars.registerHelper('$position', function(position) {
	var style = '';
	for (var key in position) {
		style += key + ": " + position[key] + "px; "
	}
	return style.trim();
});

Handlebars.registerHelper('$res', function(resource) {
	var resources = top.$.Z.Resources.Raw();
	var output = ''
	var notFound = function(resourceName) {
		console.warn("Resource " + resourceName + " not found");
	}

	if (resource && (typeof resource == 'string' || resource instanceof String)) {
		if (resources[resource]) output = resources[resource];
		else notFound(resource);
	}

	return output;
});

Handlebars.registerHelper('$mk', function(rawData, options) {
	var data = {};

	if (!options) {
		$.extend(data, rawData.hash);
	} else {
		$.extend(data, rawData);
		$.extend(data, options.hash);
	}

	var resources = top.$.Z.Resources.Raw();

	var notFound = function(resourceName) {
		console.warn("Resource " + resourceName + " not found");
		console.log(top.$.Z.Resources.Raw());
	}

	if (data.z_resource_value) {
		if (resources[data.z_resource_value]) data.value = resources[data.z_resource_value];
		else notFound(data.z_resource_value);
	}
	if (data.z_resource_title) {
		if (resources[data.z_resource_title]) data.title = resources[data.z_resource_title];
		else notFound(data.z_resource_title);
	}
	if (data.z_resource_prefix) {
		if (resources[data.z_resource_prefix]) data.prefix = resources[data.z_resource_prefix];
		else notFound(data.z_resource_prefix);
	}
	if (data.z_resource_suffix) {
		if (resources[data.z_resource_suffix]) data.suffix = resources[data.z_resource_suffix];
		else notFound(data.z_resource_suffix);
	}
	if (data.z_resource_comment) {
		if (resources[data.z_resource_comment]) data.comment = resources[data.z_resource_comment];
		else notFound(data.z_resource_comment);
	}
	if (data.z_resource_status) {
		if (resources[data.z_resource_status]) data.status = resources[data.z_resource_status];
		else notFound(data.z_resource_status);
	}

	if (!data.t)
		data.t = 'template-line'
		else if (data.t.slice(0, 9) != 'template-')
		data.t = 'template-' + data.t

	if (!data.style)
		data.style = 'day'

	if (!data.type)
		data.type = 'interactive'

	if (data.input) {
		data.input_style = data.input;
		if (data.input === 'number' || data.input === 'time' || data.input === 'date') {
			data.input_type = 'text';
			data.input_pattern = '[0-9]*';
		} else if (data.input === 'ip') {
			data.input_type = 'text';
		} else {
			data.input_type = data.input;
		}
	}

	var partial = Handlebars.partials[data.t];
	var compile = null;

	if (partial && typeof partial === 'string' || partial instanceof String) {
		compile = Handlebars.compile(partial)(data);
	} else {
		compile = partial(data)
	}
	
	var output = new Handlebars.SafeString(compile);
	output.string = output.string.trim();
	return output;
});

Handlebars.registerHelper('mkwrap', function(rawData, options) {
	var data = {};
	data.hash = {};

	if (!options) {
		$.extend(data.hash, rawData.hash);
		options = rawData;
	} else {
		$.extend(data.hash, rawData);
		$.extend(data.hash, options.hash);
	}

	var tOpen = data.hash.t + '-open'
	var tClose = data.hash.t + '-close'

	console.log(data);

	data.hash.t = tOpen;
	var wrapOpen = Handlebars.helpers.$mk(data);

	data.hash.t = tClose;
	var wrapClose = Handlebars.helpers.$mk(data);

	var block = new Handlebars.SafeString(options.fn(this));

	return wrapOpen.string + block.string + wrapClose.string;
});

Handlebars.registerHelper('repeat', function(n, block) {
	var output = '';
	for (var i = 0; i < n; ++i)
		output += block.fn(i);
	return output;
});

Handlebars.registerHelper('wrap', function(options, block) {
	var divAttrs = '';

	if (options) {
		if (options.add_class) divAttrs += 'class="' + options.add_class + '" ';
		if (options.add_attrs) divAttrs += options.add_attrs;
	}

	return new Handlebars.SafeString(
	'<div ' + divAttrs + '>'
		+ block.fn(this)
	+ '</div>');
});