var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var plumber = require('gulp-plumber');
var sourcemaps = require('gulp-sourcemaps');
var minifyCSS = require('gulp-minify-css');

gulp.task('default', ['sass', 'sass-dev'], function () {

});

gulp.task('sass', function () {
	gulp.src('css/sass/*.scss')
		.pipe(watch('css/sass/*.scss'))
		.pipe(plumber())
		.pipe(sass())
		.pipe(minifyCSS())
		.pipe(gulp.dest('./output/css'))
});

gulp.task('sass-dev', function () {
	gulp.src('css/sass/*.scss')
		.pipe(watch('css/sass/*.scss'))
		.pipe(plumber())
		.pipe(sourcemaps.init())
		.pipe(sass())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('./css'))
});