var evobj = $({});

var loadSelf = function(settings, data) {
	$("title").html(settings.template);
	$.when(
		$.get("/templates/common.html", function(reqData) {
			$("body").append(reqData);
		}),
		$.get("/templates/common_preview.html", function(reqData) {
			$("body").append(reqData);
		}),
		$.get("/dev/templates.html", function(reqData) {
			$("body").append(reqData);
		})
	).then(function() {
		initHandlebars();
		initResources();
		makeTemplate(settings, data);
		initInteractivity();
		if (settings.chart) {
			insertChart(settings.chart, data.chart_data);
		}
	});
}

var loadTemplate = function(settings, data) {	
	var templateFile = settings.template + ".html";
	var templatePath = "/templates/" + templateFile;

	$("title").html(settings.template);

	$.when(
		$.get("/templates/common.html", function(reqData) {
			$("body").append(reqData);
		}),
		$.get("/templates/common_preview.html", function(reqData) {
			$("body").append(reqData);
		}),
		$.get("/dev/templates.html", function(reqData) {
			$("body").append(reqData);
		}),
		$.get(templatePath, function(reqData) {
			$("body").append(reqData);
		})
	).then(function() {
		initHandlebars();
		initResources();
		makeTemplate(settings, data);
		initInteractivity();
		if (settings.chart) {
			insertChart(settings.chart, data.chart_data);
		}
	});
}

var loadMany = function(templates) {
	$.when(
		$.get("/templates/common.html", function(reqData) {
			$("body").append(reqData);
		}),
		$.get("/templates/common_preview.html", function(reqData) {
			$("body").append(reqData);
		}),
		$.get("/dev/templates.html", function(reqData) {
			$("body").append(reqData);
		})
	).then(function(){
		initHandlebars();
		initResources();
		makeLayout();
		templates.forEach(function(template){
			var templateFile = template.settings.template + ".html";
			var templatePath = "/templates/" + templateFile;
			$.get(templatePath, function(reqData) {
				if (!template.data) {
					template.data = {};
				}
				template.data.cdata = {};
				$.extend(template.data.cdata, cdata);
				$("body").append(reqData);
				makeFrame(template.settings, template.data);
			})
		});
	}).then(function(){
		initInteractivity();
	})
};

var makeLayout = function() {
	settings = {
		layout: 'layout-default'
	}
	var frame = Handlebars.compile($("#template-dev-frame").html());
	$("body").append(frame(settings));
}

var makeFrame = function(settings, data) {
	var templateId = "#" + settings.template.replace(/_/g, "-");
	var rawData = JSON.stringify(data, null, 4);

	var template = Handlebars.compile($(templateId).html());
	var pre = Handlebars.compile($("#template-dev-pre").html());

	var outputFrame = "";

	if (settings.outputFrame == "body") outputFrame = settings.outputFrame
		else outputFrame = ".z-style-" + settings.outputFrame

	var sourceFrame = ".z-style-" + settings.sourceFrame;

	$(outputFrame).append(template(data));
	$(sourceFrame).append(pre(settings));
	$("#hb-source").append(rawData);
}

var makeTemplate = function(settings, data) {
	var templateId = "#" + settings.template.replace(/_/g, "-");
	var rawData = JSON.stringify(data, null, 4);

	var template = Handlebars.compile($(templateId).html());
	var frame = Handlebars.compile($("#template-dev-frame").html());
	var iframe = Handlebars.compile($("#template-dev-frame-iframes").html());
	var pre = Handlebars.compile($("#template-dev-pre").html());

	//Генерация лейаута
	var layout = "";

	if (settings.layout) {
		layout = settings.layout.split("-")[1];
		if (layout == 'archive') layout = 'reports';
	} else {
		layout = 'default'
	}

	frameSettings = pageIndex;
	frameSettings.layout.layout = layout;

	if (settings.iframe == true) {
		$("body").append(iframe(frameSettings));
	} else {
		$("body").append(frame(frameSettings));
	}

	//Поиск фреймов для вставки шаблонов
	var outputFrame = "";
	var sourceFrame = "";

	if (settings.iframe == true) {
		if (settings.outputFrame == "body") outputFrame = settings.outputFrame
			else outputFrame = ".z_cls_i" + settings.outputFrame

		sourceFrame = ".z_cls_i" + settings.sourceFrame;
	} else {
		if (settings.outputFrame == "body") outputFrame = settings.outputFrame
			else outputFrame = ".z-style-" + settings.outputFrame

		sourceFrame = ".z-style-" + settings.sourceFrame;
	}

	//Вставка шаблонов
	if (settings.iframe == true) {
		$(outputFrame).contents().find('head').html('<link rel="stylesheet" href="/css/z_main.css" /> <link rel="stylesheet" href="/css/z_iframe.css" />');
		$(outputFrame).contents().find('body').html(template(data));
		$(sourceFrame).contents().find('head').html('<link rel="stylesheet" href="/css/z_main.css" /> <link rel="stylesheet" href="/css/z_iframe.css" />');
		$(sourceFrame).contents().find('body').html(pre(settings));
		$(sourceFrame).contents().find('#hb-source').append(rawData);
	} else {
		$(outputFrame).append(template(data));
		$(sourceFrame).append(pre(settings));
		$("#hb-source").append(rawData);
	}
}

var initInteractivity = function() {
	$(".z_cls_modal_close").on("click", function() {
		$(".z-style-preview-wrapper").hide();
		$(".z-style-modal-wrapper").hide();
	})

	if(settings != undefined) {
		if (settings.hideLeft == true) {
			$(".frame-left").css("display", "none");
		}
	}

	$(".z-style-button-checkbox, .z-style-button-checkbox-selected").on("click", function() {
		if ($(this).hasClass('z-style-button-checkbox')) {
			$(this).removeClass('z-style-button-checkbox')
			$(this).addClass('z-style-button-checkbox-selected')
		} else {
			$(this).removeClass('z-style-button-checkbox-selected')
			$(this).addClass('z-style-button-checkbox')
		}
	})

	$(".z-style-button-day, .z-style-button-selected-day").on("click", function() {
		if ($(this).hasClass('z-style-button-day') && !$(this).hasClass('z-style-button-plain') && !$(this).hasClass('z-dev-ignore')) {
			$(this).removeClass('z-style-button-day')
			$(this).addClass('z-style-button-selected-day')
		} else {
			$(this).removeClass('z-style-button-selected-day')
			$(this).addClass('z-style-button-day')
		}
	})

	$(".z-style-tabs-group-td-day, .z-style-tabs-group-td-selected-day").on("click", function() {
		if ($(this).hasClass('z-style-tabs-group-td-day') && !$(this).hasClass('z-dev-ignore')) {
			$(this).removeClass('z-style-tabs-group-td-day')
			$(this).addClass('z-style-tabs-group-td-selected-day')
		} else {
			$(this).removeClass('z-style-tabs-group-td-selected-day')
			$(this).addClass('z-style-tabs-group-td-day')
		}
	})

	$(".z-style-button-dimmed, .z-style-button-selected-dimmed").on("click", function() {
		if ($(this).hasClass('z-style-button-dimmed')) {
			$(this).removeClass('z-style-button-dimmed')
			$(this).addClass('z-style-button-selected-dimmed')
		} else {
			$(this).removeClass('z-style-button-selected-dimmed')
			$(this).addClass('z-style-button-dimmed')
		}
	})

	$(".z-style-button-night, .z-style-button-selected-night").on("click", function() {
		if ($(this).hasClass('z-style-button-night') && !$(this).hasClass('z-style-button-plain')) {
			$(this).removeClass('z-style-button-night')
			$(this).addClass('z-style-button-selected-night')
		} else {
			$(this).removeClass('z-style-button-selected-night')
			$(this).addClass('z-style-button-night')
		}
	})

	$(".z-style-nav-item").on("click", function() {
		lineHover = 'z-style-nav-item';
		lineSelected = 'z-style-nav-item-selected'
		if ($(this).hasClass(lineHover)) {
			$(this).removeClass(lineHover)
			$(this).addClass(lineSelected)
		} else {
			$(this).removeClass(lineSelected)
			$(this).addClass(lineHover)
		}
	})

	$(".z-style-line-hover").on("click", function() {
		lineHover = 'z-style-line-hover';
		lineSelected = 'z-style-line-selected';
		if ($(this).hasClass(lineHover)) {
			$(this).removeClass(lineHover)
			$(this).addClass(lineSelected)
		} else {
			$(this).removeClass(lineSelected)
			$(this).addClass(lineHover)
		}
	})

	$(".z-style-main-line-content-block-overflow>.z-style-interactive").on("click", function() {
		value = $(this).html();
		parent = $(this).parent()
		if(parent.next().attr('type') != undefined) {
			parent.next().removeClass("z-style-hidden");
			parent.next().val(value);
			parent.next().select();
			parent.next().focus();
			parent.addClass("z-style-hidden");
		}
	})

	$(".z-style-main-line-content-block>input").on("blur", function() {
		if($(this).attr('type') != 'search' && !$(this).hasClass('z-dev-ignore')) {
			$(this).prev().removeClass("z-style-hidden");
			$(this).addClass("z-style-hidden");
		}
	})

	$(".z_cls_change").on("click", function() {
		var editBox = $(".z_cls_editable_box")
		var boxShadow = $(editBox).next();
		var defaultState = $(".z_cls_default_buttons_state");
		var saveState = $(".z_cls_save_buttons_state");

		$(editBox).removeClass('z-style-main-line-content-block-editable')
		$(editBox).addClass('z-style-main-line-content-block-editable-edit')
		$(editBox).attr('contenteditable', true);
		$(boxShadow).hide();
		$(defaultState).addClass('z-style-hidden');
		$(saveState).removeClass('z-style-hidden');
		$(editBox).select();
		$(editBox).focus();
	})

	$(".z_cls_save").on("click", function() {
		var editBox = $(".z_cls_editable_box")
		var boxShadow = $(editBox).next();
		var defaultState = $(".z_cls_default_buttons_state");
		var saveState = $(".z_cls_save_buttons_state")

		$(editBox).scrollTop(0);
		$(editBox).removeClass('z-style-main-line-content-block-editable-edit')
		$(editBox).addClass('z-style-main-line-content-block-editable')
		$(editBox).attr('contenteditable', false);
		if ($(editBox).css('max-height') == "135px") {
			$(".z_cls_show_all").show();
			$(boxShadow).show();
		}
		$(defaultState).removeClass('z-style-hidden');
		$(saveState).addClass('z-style-hidden');
	})

	$(".z_cls_show_all").on("click", function() {
		var editBox = $(".z_cls_editable_box")
		var boxShadow = $(editBox).next();
		$(editBox).css('max-height', '');
		$(boxShadow).hide();
		$(this).hide();
	})

	// $(".z-style-frame-left, .z-style-frame-middle").on('click', function(){
	// 	var last = 'z-style-frame-last';
	// 	if ($(this).hasClass(last)) {
	// 		$(this).removeClass(last)
	// 	} else {
	// 		$(this).addClass(last)
	// 	}
	// })

	var opts = {
		lines: 9,
		length: 3,
		width: 2,
		radius: 3,
		trail: 50,
		zIndex: 0,
	};

	var spinner = $('.z-style-main-line-spinner');
	if (spinner.length != 0) {
		var spin = new Spinner(opts).spin();
		$(spinner).append(spin.el);
	}

	var bsOpts = {
			lines: 11,
			length: 5,
			width: 2,
			radius: 5,
			color: '#999999',
			trail: 50,
		};
	var bsTarget = $('.z_cls_spin_target');
	var bsSpinner = new Spinner(bsOpts).spin();
	$(bsTarget).append(bsSpinner.el)

	var slider = $('.z-style-slider-wrapper');
	if (slider.length != 0) {
		slider.noUiSlider({
			start: 0,
			range: {
				min: 0, 
				max: 100
			}
		})
	}

	var tippedOptions = {
		position: 'bottomleft', 
		fadeIn: 100,
		fadeOut: 100,
		stem: false,
		maxWidth: 400,
		container: '.z-style-frame-right-wrapper',
		containment: {
		  selector: 'body',
		  padding: 0
		},
		offset: {
			x: -10,
		}
	}
	Tipped.create('#demo-tooltip', 'За год до того как Паниковский нарушил конвенцию, проникнув в чужой эксплуатационный участок, в городе Арбатове появился первый автомобиль. Основоположником автомобильного дела был шофер по фамилии Козлевич.', tippedOptions);

	var tippedOptionsError = {
		position: 'bottomleft', 
		showOn: 'focus',
		hideOn: 'blur',
		fadeIn: 100,
		fadeOut: 100,
		stem: false,
		maxWidth: 400,
		container: '.z-style-frame-right-wrapper',
		offset: {
			y: 5
		},
	}
	Tipped.create('.z-style-input-error', 'Что-то пошло не так', tippedOptionsError);

	//Charts
	var defaultClass = 'z-style-button-day';
	var selectedClass = 'z-style-button-selected-day';

	var linearButton = $('.z-dev-chart-linear');
	var logButton = $('.z-dev-chart-log');

	var selectButton = function(button) {
		button.removeClass(defaultClass)
		button.addClass(selectedClass)
	}

	var deselectButton = function(button) {
		button.removeClass(selectedClass)
		button.addClass(defaultClass)
	}
	
	linearButton.on('click', function() {
		selectButton(linearButton);
		deselectButton(logButton);
		evobj.trigger('click_linear_button')
	})

	logButton.on('click', function() {
		selectButton(logButton);
		deselectButton(linearButton);
		evobj.trigger('click_log_button')
	})

	selectButton(linearButton);
}

var initHandlebars = function() {
	//Main
	Handlebars.registerPartial("template-nav", $("#template-nav").html());
	Handlebars.registerPartial("template-nav-item", $("#template-nav-item").html());
	Handlebars.registerPartial("template-layout", $("#template-layout").html());

	//Frames
	Handlebars.registerPartial("template-frame-spinner", $("#template-frame-spinner").html());

	//Reports
	Handlebars.registerPartial("template-reports-archive-sort", $("#template-reports-archive-sort").html());

	//Incidents
	Handlebars.registerPartial("template-reports-incident-files", $("#template-reports-incident-files").html());
	Handlebars.registerPartial("template-reports-incident-mail-channel", $("#template-reports-incident-mail-channel").html());
	Handlebars.registerPartial("template-reports-incident-mail-policy", $("#template-reports-incident-mail-policy").html());
	Handlebars.registerPartial("template-reports-incident-thumbnail", $("#template-reports-incident-thumbnail").html());
	
	//Preview
	Handlebars.registerPartial("template-preview-thumbnail-line", $("#template-preview-thumbnail-line").html());
	Handlebars.registerPartial("template-preview-content-picture", $("#template-preview-content-picture").html());
	Handlebars.registerPartial("template-preview-content-text", $("#template-preview-content-text").html());
	Handlebars.registerPartial("template-preview-content-controls", $("#template-preview-content-controls").html());

	//Controls
	Handlebars.registerPartial("template-basic-dropdown", $("#template-basic-dropdown").html());

	Handlebars.registerPartial("template-dropdown-line", $("#template-dropdown-line").html());
	Handlebars.registerPartial("template-schedule-week", $("#template-schedule-week").html());
	Handlebars.registerPartial("template-schedule-month", $("#template-schedule-month").html());
	Handlebars.registerPartial("template-schedule-year", $("#template-schedule-year").html());
	Handlebars.registerPartial("template-schedule-year-entry", $("#template-schedule-year-entry").html());

	Handlebars.registerPartial("template-slider", $("#template-slider").html());
	Handlebars.registerPartial("template-button", $("#template-button").html());
	Handlebars.registerPartial("template-button-plain", $("#template-button-plain").html());

	Handlebars.registerPartial("template-calendar", $("#template-calendar").html());
	Handlebars.registerPartial("template-calendar-day-of-week", $("#template-calendar-day-of-week").html());
	Handlebars.registerPartial("template-calendar-day", $("#template-calendar-day").html());
	Handlebars.registerPartial("template-calendar-day-plain", $("#template-calendar-day-plain").html());
	Handlebars.registerPartial("template-calendar-day-dimmed", $("#template-calendar-day-dimmed").html());

	Handlebars.registerPartial("template-tabs-wide", $("#template-tabs-wide").html());
	Handlebars.registerPartial("template-tabs-inline", $("#template-tabs-inline").html());
	Handlebars.registerPartial("template-tabs-checkbox-wide", $("#template-tabs-checkbox-wide").html());
	Handlebars.registerPartial("template-tabs-checkbox-inline", $("#template-tabs-checkbox-inline").html());
	Handlebars.registerPartial("template-tabs-tab", $("#template-tabs-tab").html());
	Handlebars.registerPartial("template-tabs-tab-checkbox", $("#template-tabs-tab-checkbox").html());

	//Common
	Handlebars.registerPartial("template-line", $("#template-line").html());
	Handlebars.registerPartial("template-line-wrap-open", $("#template-line-wrap-open").html());
	Handlebars.registerPartial("template-line-wrap-close", $("#template-line-wrap-close").html());

	Handlebars.registerPartial("template-bold-line-static", $("#template-bold-line-static").html());
	Handlebars.registerPartial("template-bold-line-interactive", $("#template-bold-line-interactive").html());

	Handlebars.registerPartial("template-head-line-static", $("#template-head-line-static").html());
	Handlebars.registerPartial("template-head-line-interactive", $("#template-head-line-interactive").html());

	Handlebars.registerPartial("template-basic-line-static", $("#template-basic-line-static").html());
	Handlebars.registerPartial("template-basic-line-interactive", $("#template-basic-line-interactive").html());
	Handlebars.registerPartial("template-basic-line-grey", $("#template-basic-line-grey").html());
	Handlebars.registerPartial("template-basic-line-expand", $("#template-basic-line-expand").html());

	Handlebars.registerPartial("template-hover-line-static", $("#template-hover-line-static").html());
	Handlebars.registerPartial("template-hover-line-interactive", $("#template-hover-line-interactive").html());
	Handlebars.registerPartial("template-hover-bold-line-static", $("#template-hover-bold-line-static").html());

	Handlebars.registerPartial("template-hover-line-expand", $("#template-hover-line-expand").html());
	Handlebars.registerPartial("template-key-line-static", $("#template-key-line-static").html());
	Handlebars.registerPartial("template-key-line-interactive", $("#template-key-line-interactive").html());
	Handlebars.registerPartial("template-key-line-editable", $("#template-key-line-editable").html());

	Handlebars.registerPartial("template-empty-line", $("#template-empty-line").html());
	Handlebars.registerPartial("template-table-line", $("#template-table-line").html());
	Handlebars.registerPartial("template-tabs-line", $("#template-tabs-line").html());
	Handlebars.registerPartial("template-multi-line", $("#template-multi-line").html());
	Handlebars.registerPartial("template-checkbox-line", $("#template-checkbox-line").html());
	Handlebars.registerPartial("template-search-line", $("#template-search-line").html());

	Handlebars.registerPartial("template-subline-wrap-open", $("#template-subline-wrap-open").html());
	Handlebars.registerPartial("template-subline-wrap-close", $("#template-subline-wrap-close").html());
	Handlebars.registerPartial("template-subline", $("#template-subline").html());
	Handlebars.registerPartial("template-key", $("#template-key").html());
	Handlebars.registerPartial("template-subline-content-static", $("#template-subline-content-static").html());

	Handlebars.registerPartial("template-subline-icons", $("#template-subline-icons").html());
	Handlebars.registerPartial("template-subline-status", $("#template-subline-status").html());

	Handlebars.registerPartial("template-value-wrap-open", $("#template-value-wrap-open").html());
	Handlebars.registerPartial("template-value-wrap-close", $("#template-value-wrap-close").html());

	Handlebars.registerPartial("template-inline", $("#template-inline").html());
	Handlebars.registerPartial("template-inline-static", $("#template-inline-static").html());
	Handlebars.registerPartial("template-inline-interactive", $("#template-inline-interactive").html());

	Handlebars.registerPartial("template-status-string", $("#template-status-string").html());
	Handlebars.registerPartial("template-shadow", $("#template-shadow").html());
	Handlebars.registerPartial("template-ghost-block", $("#template-ghost-block").html());
	Handlebars.registerPartial("template-thin-ghost-block", $("#template-thin-ghost-block").html());
	Handlebars.registerPartial("template-separator", $("#template-separator").html());
	Handlebars.registerPartial("template-cut", $("#template-cut").html());
	Handlebars.registerPartial("template-deprecated-line", $("#template-deprecated-line").html());

	Handlebars.registerPartial("template-tag-status", $("#template-tag-status").html());
	Handlebars.registerPartial("template-icon-status", $("#template-icon-status").html());
	Handlebars.registerPartial("template-icon-action", $("#template-icon-action").html());
	Handlebars.registerPartial("template-icon-action-visible", $("#template-icon-action-visible").html());
	Handlebars.registerPartial("template-icon-drag", $("#template-icon-drag").html());
	Handlebars.registerPartial("template-inline-icon", $("#template-inline-icon").html());

	//Dev
	Handlebars.registerPartial("template-dev-strike", $("#template-dev-strike").html());
	Handlebars.registerPartial("template-dev-frame", $("#template-dev-frame").html());
	Handlebars.registerPartial("template-dev-frame-iframes", $("#template-dev-frame-iframes").html());
	Handlebars.registerPartial("template-dev-pre", $("#template-dev-pre").html());
	Handlebars.registerPartial("template-dev-tooltip", $("#template-dev-tooltip").html());
	Handlebars.registerPartial("template-dev-error", $("#template-dev-error").html());
}