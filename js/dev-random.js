		var rFirstName = function() {
			var names = ["Александр", "Олег", "Андрей", "Евгений", "Григорий", "Максим", "Иван", "Артем", "Дмитрий", "Михаил"];
			return names[Math.floor(Math.random()*names.length)];
		}

		var rLastName = function() {
			var names = ["Смирнов", "Иванов", "Кузнецов", "Попов", "Соколов", "Лебедев", "Козлов", "Новиков", "Морозов", "Петров"];
			return names[Math.floor(Math.random()*names.length)];
		}

		var rIcon = function() {
			var icons = ["fa-envelope", "fa-apple", "fa-facebook", "fa-windows", "fa-file", "fa-rocket", "fa-user", "fa-send", "fa-database", "fa-anchor"];
			return icons[Math.floor(Math.random()*icons.length)];
		}

		var rMorePeople = function() {
			var number = Math.floor(Math.random()*7 + 1);
			if (number <= 3) {
				return "и еще " + number;
			} else {
				return ""
			}
		}

		var rTime = function() {
			var hours = Math.floor(Math.random()*11 + 10).toString();
			var minutes = Math.floor(Math.random()*23 + 1).toString();
			if (minutes.length == 1) {
				minutes = "0" + minutes;
			}
			return hours + ":" + minutes
		}

		var rBool = function() {
			return Math.random()<.5
		}

		var rPolicy = function() {
			var policies = ["Общая политика", "Общая политика", "Общая политика", "Странная политика", "Поиск работы", "Подозрительные файлы", "Секреты", "Черный список"];
			return policies[Math.floor(Math.random()*policies.length)];
		}

		var rAction = function() {
			var actions = ["блокировка", "создание инцидента", "создание инцидента", "создание инцидента"];
			return actions[Math.floor(Math.random()*actions.length)];
		}

		var rLine = function() {
			var lines = ["Пешеходов надо любить. Пешеходы составляют большую часть человечества. Мало того — лучшую его часть. Пешеходы создали мир.",
				 "Надо заметить, что автомобиль тоже был изобретён пешеходами. Но автомобилисты об этом как-то сразу забыли. Кротких и умных пешеходов стали давить. Улицы, созданные пешеходами, перешли во власть автомобилистов.", 
				 "В большом городе пешеходы ведут мученическую жизнь. Для них ввели некое транспортное гетто. Им разрешают переходить улицы только на перекрёстках, то есть именно в тех местах, где движение сильнее всего и где волосок, на котором обычно висит жизнь пешехода, легче всего оборвать.", 
				 "Если пешеходу иной раз удаётся выпорхнуть из-под серебряного носа машины — его штрафует милиция за нарушение правил уличного катехизиса.",
				 "Корейко понял, что сейчас возможна только подземная торговля, основанная на строжайшей тайне. Все кризисы, которые трясли молодое хозяйство, шли ему на пользу, все, на чем государство теряло, приносило ему доход.", 
				 "Холодные яйца всмятку — еда очень невкусная, и хороший, весёлый человек никогда их не станет есть.", 
				 "Александр Иванович не ел, а питался. Он не завтракал, а совершал физиологический процесс введения в организм должного количества жиров, углеводов и витаминов.",
				 "В том, что старое вернётся, Корейко никогда не сомневался. Он берег себя для капитализма.", 
				 "Параллельно большому миру, в котором живут большие люди и большие вещи, существует маленький мир с маленькими людьми и маленькими вещами.", 
				 "Проклятая страна! Страна, в которой миллионер не может повести свою невесту в кино.",
				 "Есть люди, которые не умеют страдать, как-то не выходит. А если уж и страдают, то стараются проделать это как можно быстрее и незаметнее для окружающих.", 
				 "Вы знаете, Бендер, как я ловлю гуся? Я убиваю его как тореадор, — одним ударом! Это опера, когда я иду на гуся! «Кармен»!", 
				 "Паниковский вас всех продаст, купит и снова продаст… но уже дороже"];
			return lines[Math.floor(Math.random()*lines.length)];
		}

		var rFullName = function() {
			return rFirstName() + " " + rLastName();
		}

		var makeMockArchiveEntry = function() {
			var entry = {};
			entry.type = "email";
			entry.icon = rIcon();
			entry.mainLine = rFirstName() + " " + rLastName() + " → " + rFirstName() + " " + rLastName() + " " + rMorePeople();
			entry.date = rTime();
			entry.paperclip = rBool();
			entry.tag = rBool();
			entry.policy = rPolicy();
			entry.action = rAction();
			entry.secondLine = rLine();
			return entry;
		}