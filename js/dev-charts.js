var insertChart = function(name, data) {
	window[name + 'Chart'](data)
}

var basicChart = function(data) {

	// *** *** *** *** ***
	// Constants
	// *** *** *** *** ***

	var margin = {top: 20, right: 20, bottom: 20, left: 20},
		svgWidth = 900 - margin.left - margin.right,
		svgHeight = 400 - margin.top - margin.bottom,
		textHeight = 30,
		xScaleHeight = 30,
		textOffset = 5,
		barWidth = Math.floor((svgWidth - 1 * data.length) / data.length),
		chartWidth = (data.length * (barWidth + 1) - 1)
		barHeight = svgHeight - xScaleHeight;

	// *** *** *** *** ***
	// Canvas
	// *** *** *** *** ***
	
	var svg = d3.select('#chart-wrapper').append('svg')
		.attr('width', svgWidth + margin.left + margin.right)
		.attr('height', svgHeight + margin.top + margin.bottom)
		.append('g')
		.attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

	// *** *** *** *** ***
	// Events
	// *** *** *** *** ***

	evobj.on('click_linear_button', function(){
		update(yLinear);
	})

	evobj.on('click_log_button', function(){
		update(yLog);
	})

	var update = function(scaleX) {
		barRect
			.transition()
			.attr('transform', function(d, i) { 
				return 'translate(0, ' + (svgHeight - scaleX(d.number) - xScaleHeight) + ')';
			})
			.attr('height', function(d){ return scaleX(d.number) })

		// barLink
		// 	.transition()
		// 	.attr('y', function(d) { return svgHeight - scaleX(d.number) - xScaleHeight - textHeight/2 + textOffset; })
	}

	// *** *** *** *** ***
	// Scales
	// *** *** *** *** ***

	var yLinear = d3.scale.linear()
		.domain([0, d3.max(data, function(d){ return d.number; })])
		.range([0, barHeight]);

	var	yLog = d3.scale.log()
		.domain([1, d3.max(data, function(d){ return d.number; })])
		.range([0, barHeight]);

	var startDate = new Date();
	startDate.setDate(startDate.getDate() + 46);
	var endDate = new Date();
	endDate.setDate(endDate.getDate() + data.length - 2 + 46);

	var xDateScale = d3.time.scale()
		.domain ([startDate, endDate])
		.range([barWidth/2, chartWidth - barWidth/2])
		.nice(d3.time.day)

	// *** *** *** *** ***
	// X Axis
	// *** *** *** *** ***

	// Форматтер для первого тика на оси, который всегда должен отображать месяц
	var firstDayFormatter = ["%-d %b", function(d) { return (d.getDate() == startDate.getDate()) && (d.getMonth() == startDate.getMonth()) }];

	// То же самое, но не показывает число
	var firstDayFormatterMonth = ["%b", function(d) { return (d.getDate() == startDate.getDate()) && (d.getMonth() == startDate.getMonth()) }];

	// То же самое, но для второго дня (для xTickCount = 2)
	var secondDay = new Date(startDate.getTime());
	secondDay.setDate(startDate.getDate() + 1);
	var secondDayFormatterMonth = ["%b", function(d) { return (d.getDate() == secondDay.getDate()) && (d.getMonth() == secondDay.getMonth()) }];

	// Варианты форматирования:

	// Дни недели, отмечен каждый день
	var xTickInterval = d3.time.day;
	var xTickCount = 1;
	var xTickFormat = d3.time.format('%a %-d');

	// Число, первый день каждого месяца и первый день периода отмечены с месяцем. Далее то же самое, но меньше тиков
	if (data.length > 7) {
		xTickCount = 1;
		xTickFormat = d3.time.format.multi([
			firstDayFormatter,	
			["%b", function(d) { return d.getDate() == 1; }],
			["%-d", function() { return true }]
		]);
	}

	if (data.length > 30) {
		xTickCount = 2;
		xTickFormat = d3.time.format.multi([
			firstDayFormatterMonth,
			secondDayFormatterMonth,
			["", function(d) { return (d.getDate() == 31 || (d.getDate() == 29 && d.getMonth() == 2)) }],
			["%b", function(d) { return d.getDate() == 1; }],
			["%-d", function() { return true }]
		]);
	}

	if (data.length > 80) {
		xTickCount = 3;
	}

	if (data.length > 120) {
		xTickCount = 5;
		xTickFormat = d3.time.format.multi([
			["", function(d) { return (d.getDate() == 31) }],
			["%b", function(d) { return d.getDate() == 1; }],
			["%-d", function() { return true }]
		]);
	}

	var xDateAxis = d3.svg.axis()
		.scale(xDateScale)
		.orient('bottom')
		.ticks(xTickInterval, xTickCount)
		.tickFormat(xTickFormat)

	// *** *** *** *** ***
	// Y Axis
	// *** *** *** *** ***

	var yNumberAxis = d3.svg.axis()
		.scale(yLinear)
		.orient('right')
		// .ticks(xTickInterval, xTickCount)
		// .tickFormat(xTickFormat)

	// *** *** *** *** ***
	// SVG Render
	// *** *** *** *** ***

	var xDateAxisSvg = svg
		.append('g')
		.classed('z-style-svg-axis', true)
		.attr('transform', 'translate(0, ' + barHeight + ')')
		.call(xDateAxis)

	var xDateAxisSvg = svg
		.append('g')
		.classed('z-style-svg-axis', true)
		.attr('transform', 'translate(0, ' + barHeight + ')')
		.call(yNumberAxis)

	var bar = svg
		.append('g')
		.selectAll('g')
			.data(data)
		.enter().append('g')
		.append('g')
			.attr('transform', function(d, i) { 
				return 'translate(' + i * (barWidth + 1) + ', 0)';
			})
			.style('height', function(d) { return svgHeight + 'px'; })
			.classed('z-style-svg-bar', true)

	var barRectBg = bar.append('rect')
		.attr('height', function(d) { return svgHeight + 'px'; })
		.attr('width', barWidth)

	var barRect = bar.append('rect')
		.attr('transform', function(d, i) { 
			return 'translate(0, ' + (svgHeight - yLinear(d.number) - xScaleHeight) + ')';
		})
		.attr('height', function(d){ return yLinear(d.number) })
		.attr('width', barWidth)
		.classed('z-style-svg-bar-rect', true)

	// var barLink = bar.append('svg:a')
	// 	.attr('xlink:href', function(d){ return d.url; })
	// 	.classed('z-style-svg-interactive', true)
	// 		.append('text')
	// 		.text(function(d) { return d.number; })
	// 		.attr('y', function(d) { return svgHeight - yLinear(d.number) - xScaleHeight - textHeight/2 + textOffset; })
	// 		.attr('x', barWidth / 2)
	// 		.attr('text-anchor', 'middle')
}