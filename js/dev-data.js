var pageReferencesLeft = {
	settings: {
		template: "template_dev_references_left",
		outputFrame: "frame-left",
	}
}

var pageIndex = {
	nav: {
		add_class: "z_cls_wrapper",
		add_attrs: "attrs",
		menu: {
			add_line_class: "z_cls",
			add_line_attrs: "foo='bar'",
			content: [
				{
					add_class: "z_cls",
					add_attrs: "foo='bar'",
					value: "Рабочий стол",
				},
				{
					value: "Отчеты"
				},
				{
					value: "Справочники"
				},
				{
					value: "Политики"
				},
				{
					value: "Настройки"
				},
				{
					value: "Помощь"
				}
			]
		},
		upload: {
			add_line_class: "z_line_cls",
			add_line_attrs: "foo='bar'",
			count: {
				add_class: "z_cls",
				add_attrs: "foo='bar'",
				icon: "z_cls_icon_cloud_upload",
				value: "9"
			},
			save: {
				add_class: "z_cls",
				add_attrs: "foo='bar'",
			}
		},
		notifications: {
			add_line_class: "z_line_cls",
			add_line_attrs: "foo='bar'",
			icon: "z_cls_icon_flag",
			add_class: "z_cls",
			add_attrs: "foo='bar'",
			value: "5"
		},
		warnings: {
			add_line_class: "z_line_cls",
			add_line_attrs: "foo='bar'",
			icon: "z_cls_icon_warning",
			add_class: "z_cls",
			add_attrs: "foo='bar'",
			value: "3"
		},
		user: {
			add_class: "z_cls",
			add_attrs: "foo='bar'",
			icon: "z_cls_icon_user",
			value: "Константин Константинович Константинопольский"
		},
		search: {
			add_class: "z_cls",
			add_attrs: "foo='bar'",
			icon: "z_cls_icon_search"
		}
	},
	layout: {
		add_class: "z_cls_main",
		add_attrs: "foo='bar'",
		frame_left: {
			add_class: "z_cls_iframe-left"
		},
		frame_middle: {
			add_class: "z_cls_iframe-middle"
		},
		frame_right: {
			add_class: "z_cls_iframe-right"
		}
	}
}

var pageMk = {
	icon_data: {
		pre_statuses: [
			{
				icon: "z_cls_icon_mail"
			}
		]
	},
	full_data: {
		input: 'text',
		wrap: true,
		hover: true,
		value: "Тоже длинная строка, которая очень плохо влезает в отведенное ей место",
		title: "Строка неплохой такой длинны, уж точно не на одну строку",
		status: "Статус",
		add_class: "z_cls_value",
		add_attrs: "foo='bar'",
		add_line_class: "z_cls_line",
		add_line_attrs: "bar='foo'",
		pre_statuses: [
			{	
				add_class: "z_cls_status_mail",
				add_attrs: "foo='bar'",
				icon: "z_cls_icon_mail"
			}
		],
		pre_actions: [
			{	
				add_class: "z_cls_action_collapsed",
				add_attrs: "foo='bar'",
				icon: "z_cls_icon_collapsed"
			}
		],
		statuses: [
			{
				tag: "orange"
			},
			{
				tag: "darkred"
			},
			{	
				add_class: "z_cls_status_class",
				add_attrs: "foo='bar'",
				icon: "z_cls_icon_flag"
			},
			{	
				add_class: "z_cls_status_class",
				add_attrs: "foo='bar'",
				icon: "z_cls_icon_warning"
			}
		],
		actions: [
			{	
				add_class: "z_cls_action_class",
				add_attrs: "foo='bar'",
				icon: "z_cls_icon_delete"
			}
		],
	}
}

var pagePolygon = {
	mk_action: {
		actions: [
			{
				icon: "z_cls_icon_delete"
			}
		]
	},
	mk_pre_statuses: {
		pre_statuses: [
			{	
				icon: "z_cls_icon_mail"
			},
			{
				tag: "orange"
			}
		],
	},
	mk_pre_actions: {
		pre_actions: [
			{	
				icon: "z_cls_icon_menu"
			},
			{
				icon: "z_cls_icon_delete"
			}
		],
	},
	mk_statuses: {
		statuses: [
			{	
				icon: "z_cls_icon_mail"
			},
			{
				tag: "orange"
			}
		],
	},
	mk_actions: {
		actions: [
			{	
				icon: "z_cls_icon_menu"
			},
			{
				icon: "z_cls_icon_delete"
			}
		],
	},
	mk_all_icons: {
		pre_statuses: [
			{	
				icon: "z_cls_icon_mail"
			},
			{
				tag: "orange"
			}
		],
		pre_actions: [
			{	
				icon: "z_cls_icon_menu"
			},
			{
				icon: "z_cls_icon_delete"
			}
		],
		statuses: [
			{	
				icon: "z_cls_icon_mail"
			},
			{
				tag: "orange"
			}
		],
		actions: [
			{	
				icon: "z_cls_icon_menu"
			},
			{
				icon: "z_cls_icon_delete"
			}
		]
	},
	mk_button: {
		button: {
			value: "Кнопка!",
			icon: "z_cls_icon_mail"
		}
	},
	head_line_interactive: {
		value: "Интерактивный заголовок секции (head-line-interactive)"
	},
	head_line_static: {
		value: "Статичный заголовок секции (head-line-static)",
		actions: [
			{	
				add_class: "z_cls_action_class",
				add_attrs: "foo='bar'",
				icon: "z_cls_icon_add"
			}
		],
	},
	line_states: {
		icon: "z_cls_icon_flag",
		value: "Строка с кучей состояний",
		states: [
			{
				add_class: "z_line_cls",
				add_attrs: "foo='bar'",
				icon: "z_cls_icon_warning",
				value: "Первое"
			},
			{
				value: "Второе"
			},
			{
				value: "И третье"
			}
		]
	},
	tabs_wide: {
		content: [
			{
				value: "tabs-line"
			},
			{
				value: "Это табы!"
			},
			{
				value: "Невероятно сверхдлинная строка, которая очень плохо влезает в отведенное ей место"
			}
		],
		actions: [
			{
				icon: "z_cls_icon_add",
			}
		]
	},
	multi_line: {
		value: "Статичный многострочный текст: «За год до того как Паниковский нарушил конвенцию, проникнув в чужой эксплуатационный участок, в городе Арбатове появился первый автомобиль. Основоположником автомобильного дела был шофер по фамилии Козлевич.» (multi-line)"
	},
	search_line: {
		value: "Строка поиска (search-line)"
	},
	key_line_editable: {
		title: "key-line-editable",
		add_class: "z_cls_editable_box",
		value: "За год до того как Паниковский нарушил конвенцию, проникнув в чужой эксплуатационный участок, в городе Арбатове появился первый автомобиль. Основоположником автомобильного дела был шофер по фамилии Козлевич. К рулевому колесу его привело решение начать новую жизнь. Старая жизнь Адама Козлевича была греховна. Он беспрестанно нарушал уголовный кодекс РСФСР, а именно статью 162-ю, трактующую вопросы тайного похищения чужого имущества (кража). Статья эта имеет много пунктов, но грешному Адаму был чужд пункт «а» (кража, совершенная без применения каких-либо технических средств). Это было для него слишком примитивно. Пункт «д», карающий лишением свободы на срок до пяти лет, ему также не подходил. Он не любил долго сидеть в тюрьме. И так как с детства его влекло к технике, то он всею душою отдался пункту «в» (тайное похищение чужого имущества, совершенное с применением технических средств или неоднократно, или по предварительному сговору с другими лицами, а равно, хотя и без указанных условий, совершенное на вокзалах, пристанях, пароходах, вагонах и в гостиницах).",
		default_buttons: {
			add_class: "z_cls_default_buttons_state",
			content: [
				{
					value: "Изменить",
					add_class: "z_cls_change",
				},
				{
					value: "Показать все",
					add_class: "z_cls_show_all"
				}
			]
		},
		save_buttons: {
			add_class: "z_cls_save_buttons_state",
			content: [
				{
					value: "Сохранить",
					add_class: "z_cls_save",
					comment: "Shift+Enter"
				}
			]
		}
	},
	table_line: {
		add_line_class: "heyhey",
		cell1: {
				value: "1/4"
		},
		cell2: {
				value: "За год до того как Паниковский нарушил конвенцию, проникнув в чужой эксплуатационный участок, в городе Арбатове появился первый автомобиль."
		},
		cell3: {
				value: "1/2",
				actions: [
					{
						icon: "z_cls_icon_menu"
					}
				]
		},
		cells: [
			{
				static_line: true,
				part: 3,
				value: "1/4"
			},
			{
				static_line: true,
				part: 3,
				wrap: true,
				value: "За год до того как Паниковский нарушил конвенцию, проникнув в чужой эксплуатационный участок, в городе Арбатове появился первый автомобиль."
			},
			{
				interactive_line: true,
				part: 6,
				value: "1/2",
				input_style: 'text',
				type: 'interactive',
				actions: [
					{
						icon: "z_cls_icon_menu"
					}
				]
			}
		]
	},
	input: {
		value: "Различные инупуты, следует передавать эти строки в input_style"
	},
	input_text: {
		title: "text",
		value: "Невероятно сверхдлинная строка, которая очень плохо влезает в отведенное ей место"
	},
	input_short_text: {
		title: "short-text",
		value: "Небольшой текст, например, имя пользователя"
	},
	input_number: {
		title: "number",
		value: "100",
		suffix: "МБ"
	},
	input_email: {
		title: "email",
		value: "alex.tewpin@gmail.com"
	},
	input_date: {
		title: "date",
		value: "01.12.14"
	},
	input_time: {
		title: "time",
		value: "18:42"
	},
	input_ip: {
		title: "ip",
		value: "192.186.000.001"
	},
	input_url: {
		title: "url",
		value: "google.com"
	},
	input_tel: {
		title: "tel",
		value: "+7 916 319 98 28"
	},
	input_password: {
		title: "password",
		value: "Изменить"
	},
	input_full: {
		hover: true,
		value: "Невероятно сверхдлинная строка, которая очень плохо влезает в отведенное ей место. Более того, она может ресползаться даже на несколько линий! Крипи.",
		title: "Строка неплохой такой длинны, уж точно не на одну строку",
		status: "статус",
		input: 'text',
		icon: "z_cls_icon_mail",
		add_class: "z_cls_value",
		add_attrs: "foo='bar'",
		add_line_class: "z_cls_line",
		add_line_attrs: "bar='foo'",
		pre_statuses: [
			{	
				add_class: "z_cls_status_mail",
				add_attrs: "foo='bar'",
				icon: "z_cls_icon_mail"
			}
		],
		pre_actions: [
			{	
				add_class: "z_cls_action_collapsed",
				add_attrs: "foo='bar'",
				icon: "z_cls_icon_collapsed"
			}
		],
		statuses: [
			{
				tag: "orange"
			},
			{
				tag: "darkred"
			},
			{	
				add_class: "z_cls_status_class",
				add_attrs: "foo='bar'",
				icon: "z_cls_icon_flag"
			},
			{	
				add_class: "z_cls_status_class",
				add_attrs: "foo='bar'",
				icon: "z_cls_icon_warning"
			}
		],
		actions: [
			{	
				add_class: "z_cls_action_class",
				add_attrs: "foo='bar'",
				icon: "z_cls_icon_delete"
			}
		]
	},
	empty_line: {
		icon: "z_cls_icon_mail",
		add_class: "z_cls_value",
		add_attrs: "foo='bar'",
		add_line_class: "z_cls_line",
		add_line_attrs: "bar='foo'",
		statuses: [
			{	
				icon: "z_cls_icon_flag"
			},
			{	
				icon: "z_cls_icon_warning"
			}
		],
		actions: [
			{	
				icon: "z_cls_icon_menu"
			}
		],
		status: "empty-line, контейнер для контрола"
	},
	make: {
		value: "{{$make 'partial' data title value ghost input_style style}}"
	},
	make_partial: {
		title: "partial",
		value: "Строка с именем шаблона"
	},
	make_data: {
		title: "data",
		value: "Объект с данными"
	},
	make_title: {
		title: "title",
		value: "Строка с именем ресурса для ключа (только для строк ключ-значение)"
	},
	make_value: {
		title: "value",
		value: "Строка с именем ресурса для значения"
	},
	make_ghost: {
		title: "ghost",
		value: "Число с количеством блоков для отступа"
	},
	make_input_style: {
		title: "input_style",
		value: "Строка с типом текстового инпута. Без нее инпут не сгенерируется"
	},
	make_style: {
		title: "style",
		value: "Строка со стилем (используется редко)"
	},
	line: {
		value: "Любая линия принимает эти атрибуты в объекте data"
	},
	line_add_class: {
		title: "add_class",
		value: "Строка с классами для значения"
	},
	line_add_attrs: {
		title: "add_attrs",
		value: "Строка с атрибутами для значения"
	},
	line_add_line_class: {
		title: "add_line_class",
		value: "Строка с классами для всей линии"
	},
	line_add_line_attrs: {
		title: "add_line_attrs",
		value: "Строка с атрибутами для всей линии"
	},
	line_button: {
		title: "button",
		value: "Объект с кнопкой. Кнопка не входит в стейт"
	},
	line_spinner: {
		title: "spinner",
		value: "Булево, вставляет контейнер для крутилки"
	},
	line_link: {
		title: "link",
		value: "Строка со ссылкой для линий hover-типа"
	},
	line_icon: {
		title: "icon",
		value: "Строка с именем класса для иконки"
	},
	line_value: {
		title: "value",
		value: "Строка со значением"
	},
	line_z_res_value: {
		title: "z_resource_value",
		value: "Строка с именем ресурса для значения",
	},
	line_prefix: {
		title: "prefix",
		value: "Строка со префиксом"
	},
	line_z_res_prefix: {
		title: "z_resource_prefix",
		value: "Строка с именем ресурса для префикса",
	},
	line_suffix: {
		title: "suffix",
		value: "Строка с суффиксом"
	},
	line_z_res_suffix: {
		title: "z_resource_suffix",
		value: "Строка с именем ресурса для суффикса",
	},
	line_comment: {
		title: "comment",
		value: "Строка с комментарием"
	},
	line_z_res_comment: {
		title: "z_resource_comment",
		value: "Строка с именем ресурса для комментария",
	},
	line_pre_statuses: {
		title: "pre_statuses",
		value: "Массив с иконками статусов в начале строки"
	},
	line_pre_actions: {
		title: "pre_actions",
		value: "Массив с иконками действий в начале строки"
	},
	line_statuses: {
		title: "statuses",
		value: "Массив с иконками статусов в конце строки"
	},
	line_status: {
		title: "status",
		value: "Объект со строкой статуса в конце строки"
	},
	line_actions: {
		title: "actions",
		value: "Массив с иконками действий"
	},
	key_line: {
		value: "Дополнительные атрибуты для линий типа ключ-значение"
	},
	key_line_title: {
		title: "title",
		value: "Строка с ключем"
	},
	key_line_z_res_title: {
		title: "z_resource_title",
		value: "Строка с именем ресурса для ключа"
	},
	bold_line_static: {
		value: "Заголовок блока (bold-line-static)"
	},
	bold_line_interactive: {
		value: "bold-line-interactive"
	},
	basic_line_static: {
		value: "Однострочный текст (basic-line-static)"
	},
	basic_line_interactive: {
		value: "Интерактивный элемент на всю линию (basic-line-interactive)"
	},
	basic_line_expand: {
		value: "basic-line-expand"
	},
	hover_line_static: {
		draggable: true,
		value: "Строка с ховером (hover-line-static)",
		comment: "у этой линии есть параметр link",
		link: "#",
		actions: [
			{
				icon: "z_cls_icon_delete"
			}
		],
		pre_actions: [
			{
				icon: "z_cls_icon_collapsed"
			}
		]
	},
	hover_bold_line_static: {
		draggable: true,
		value: "Заголовок с ховером (hover-bold-line-static)",
		comment: "тоже бывает с комментарием",
		actions: [
			{
				icon: "z_cls_icon_delete"
			}
		]
	},
	hover_line_interactive: {
		value: "Элемент списка с возможностью редактирования (hover-line-interactive)",
		actions: [
			{
				icon: "z_cls_icon_delete"
			}
		]
	},
	hover_line_expand: {
		value: "Заголовок раскрывающегося списки (hover-line-expand)",		
		actions: [
			{
				icon: "z_cls_icon_delete"
			}
		]
	},
	key_line_static: {
		value: "Статичная линия ключ-значение",
		title: "key-line-static",
		comment: "невероятно длинный комментарий, который не влезает в строку"
	},
	key_line_interactive: {
		value: "Интерактивная линия ключ-значение",
		title: "key-line-interactive",
		comment: "невероятно длинный комментарий, который не влезает в строку"
	},
	spinner: {
		prefix: "Процесс идет.",
		value: "Остановить",
		title: "Кнопка и спиннер",
		spinner: true,
		button: {
			value: "Поехали!"
		}
	},
	checkbox_line: {
		value: "Чекбокс (checkbox-line)",
		add_class: "z_cls_value",
		add_attrs: "foo='bar'",
	},
	button_line: {
		title: "Кнопка",
		value: "Встраивается в практически любую линию",
		add_class: "z_cls_value",
		add_attrs: "foo='bar'",
	},
	prefix_suffix: {
		title: "Строка с префиксом и суффиксом",
		value: "значение",
		prefix: "Префикс",
		suffix: "суффикс",
		comment: "невероятно длинный комментарий, который не влезает в строку"
	},
	element: {
		value: "Невероятно сверхдлинная строка, которая очень плохо влезает в отведенное ей место",
		title: "Строка неплохой такой длинны, уж точно не на одну строку",
		icon: "z_cls_icon_mail",
		add_class: "z_cls_value",
		add_attrs: "foo='bar'",
		add_line_class: "z_cls_line",
		add_line_attrs: "bar='foo'",
		statuses: [
			{	
				add_class: "z_cls_status_class",
				add_attrs: "foo='bar'",
				icon: "z_cls_icon_flag"
			},
			{	
				add_class: "z_cls_status_class",
				add_attrs: "foo='bar'",
				icon: "z_cls_icon_warning"
			}
		],
		actions: [
			{	
				add_class: "z_cls_action_class",
				add_attrs: "foo='bar'",
				icon: "z_cls_icon_delete"
			}
		],
		status: {
			value: "статус",
			add_class: "z_cls_text_status_class",
			add_attrs: "foo='bar'",
		}
	},
	dont_use: {
		value: "Эти элементы пока не используются и существуют для консистентности"
	},
	rare_use: {
		value: "Эти элементы используются в единичных случаях"
	}
};

var cdata = {
	iconMenu: {
		actions: [
			{
				icon: "z_cls_icon_menu"
			}
		]
	}
}

var globalResources = {
	"r_settings_left_server_title":"Серверные",
	"r_settings_left_spread_title":"Распостраняемые",
	"r_settings_event_reactions_title":"Реакция на события",
	"r_settings_archive_connection_title":"Соединение",
	"r_preview_cut": "Размер текстового содержимого файла превышает 512 Кб, сохраните его чтобы просмотреть полностью",
	"r_reports_incident_details": "Детали инцидента",
	"r_common_server":"Сервер",
	"r_common_db":"База данных",
	"r_common_user_type":"Тип пользователя",
	"r_common_user":"Пользователь",
	"r_common_password":"Пароль",
	"r_common_check_connection":"Проверить соединение",
	"r_common_m_ending":"Не задан",
	"r_common_f_ending":"Не задана",
	"r_common_n_ending":"Не задано",
	"r_common_p_ending":"Не заданы",
	"r_settings_archive_title": "Настройки архива",
	"r_settings_discovery_title": "Настройки сервера Discovery",
	"r_settings_discovery_db_title": "Базы данных, сканируемые агентом",
	"r_settings_ocr_general_title": "Настройки выделения текста из изображений",
	"r_common_save_all": "Сохранить все",
	"r_common_search": "Поиск",
	"r_common_check_all": "Отметить все",
	"r_common_first": "Первый",
	"r_common_days_of_week": "Дни недели",
	"r_common_days_of_month": "Числа",
	"r_common_sender": "Отправитель",
	"r_common_receiver": "Получатель",
	"r_common_and": "и",
	"r_common_recipients": "Получатели",
	"r_common_preview": "Препросмотр",
	"r_common_text": "Текст",
	"r_common_slide_show": "Слайд-шоу",
	"r_common_save": "Сохранить",
	"r_common_access": "Доступ",
	"r_common_licenses": "Лицензии",
	"r_common_users": "Пользователи",
	"r_common_ocr_core": "Ядро OCR",
	"r_common_language": "Язык",
	"r_common_tesseract": "Tesseract",
	"r_common_russian": "Русский",
	"r_common_test": "Проверить",
	"r_common_range": "Область применения",
	"r_common_pcs": "Компьютеры",
	"r_common_server": "Сервер",
	"r_common_folders": "Папки",
	"r_common_settings": "Настройки",
	"r_common_general_settings": "Общие настройки",
	"r_common_advanced_settings": "Дополнительно",
	"r_common_exchange": "Exchange",
	"r_common_dbs": "Базы данных",
	"r_common_db_string": "Cтрока соединения с базой данных",
	"r_common_query": "Запрос",
	"r_common_key_column": "Ключевая колонка",
	"r_common_data_column": "Колонка с данными",
	"r_common_data_type": "Тип данных",
	"r_common_data_encoding": "Кодировка текстовых данных",
	"r_common_schedule": "Расписание",
	"r_common_test_button": "Проверить",
	"r_common_user_type": "Тип пользователя",
	"r_common_mb": "МБ",
	"r_common_jan":"Янв",
	"r_common_feb":"Фев",
	"r_common_mar":"Март",
	"r_common_apr":"Апр",
	"r_common_may":"Май",
	"r_common_jun":"Июнь",
	"r_common_jul":"Июль",
	"r_common_aug":"Авг",
	"r_common_sep":"Сент",
	"r_common_oct":"Окт",
	"r_common_nov":"Нояб",
	"r_common_dec":"Дек",
	"r_common_mon":"ПН",
	"r_common_tues":"ВТ",
	"r_common_wed":"СР",
	"r_common_thur":"ЧТ",
	"r_common_frid":"ПТ",
	"r_common_sat":"СБ",
	"r_common_sun":"ВС",
	"r_common_monday":"Понедельник",
	"r_db_user_types":
	[
		{
			"value":"0",
			"name": "r_common_server"
		},
		{
			"value":"1",
			"name":"Microsoft Windows"
		}
	]
};

var initResources = function() {
	var raw = function(){
		return globalResources;
	}

	top.$.Z = {};
	top.$.Z.Resources = {};
	top.$.Z.Resources.Raw = raw;
}